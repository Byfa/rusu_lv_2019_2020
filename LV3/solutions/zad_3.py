# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 22:52:06 2021

@author: nagy
"""

# Zadatak 3
# Na stranici http://iszz.azo.hr/iskzl/exc.htm moguće je dohvatiti podatke o kvaliteti zraka za Republiku Hrvatsku. Podaci
# se mogu preuzeti korištenjem RESTfull servisa u XML ili JSON obliku. U rusu_lv_2019_20/LV3/resources/
# skriptu koja dohvaća podatke te ih pohranjuje u odgovarajući DataFrame. Prepravite/nadopunite skriptu s
# programskim kodom kako bi dobili sljedeće rezultate:
# 1. Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek.
# 2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
# 3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
# 4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
# 5. Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda.

import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

# url that contains valid xml file:
#1. Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek.
url = "http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=02.01.2017&vrijemeDo=01.01.2018"

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

print(root)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme');  #prikazuje podatke za cijelu godinu

# add date month and day designator
df['month'] = pd.DatetimeIndex(df['vrijeme']).month  #u dataFrame dodaje stupce month i dayOfweek
df['dayOfweek'] = pd.DatetimeIndex(df['vrijeme']).dayofweek

#2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
najveceVrijednosti = df.sort_values(by=['mjerenje'], ascending=False) #sortiramo dataFrame po vrijednosti mjerenja [PM10]
print("\nTri datuma kad je koncentracija PM10 u 2017. bila najveca: ")#ascending=False -- od najvece prema najmanjoj
print(najveceVrijednosti['vrijeme'].head(3))  #ispisujemo 3 datuma sa vrha sortiranog dataFrame-a

#3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
index=np.arange(1,13,1)
prazni=[]
for i in range(1,13):   #za svaki mjesec provjeravamo
    mjesec= df[df.month==i]
    if (i==2):
        prazni.append(28-len(mjesec)) 
    elif (i==4) or (i==6) or(i==9) or(i==11):
        prazni.append(30-len(mjesec)) 
    else:
        prazni.append(31-len(mjesec))

plt.figure()
plt.bar(index, prazni)
plt.title("Broj izostalih vrijednosti tijekom mjeseci")
plt.xlabel("mjesec")
plt.ylabel("broj dana bez unosa mjerenja")
plt.grid(linestyle="--")

#4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
konc_ozujak=[]
konc_studeni=[]
ozujak=df[df.month==3]
studeni=df[df.month==11]

for i in ozujak["mjerenje"]:
    konc_ozujak.append(i)    
    
for i in studeni["mjerenje"]:   
    konc_studeni.append(i)   
    
plt.figure()
plt.boxplot([konc_ozujak, konc_studeni], positions = [3,11]) 
plt.title("Koncentracija PM10 u ozujku i studenom")
plt.xlabel("Mjesec")
plt.ylabel("Koncentracija")
plt.grid(axis='y',linestyle="-")

#5. Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda.
radni_tj=[]
vikend=[]
radni=df[(df.dayOfweek==0)|(df.dayOfweek==1)|(df.dayOfweek==2)|(df.dayOfweek==3)|(df.dayOfweek==4)]
vik=df[(df.dayOfweek==5)|(df.dayOfweek==6)]
        
for i in radni["mjerenje"]:
    radni_tj.append(i)
for i in vik["mjerenje"]:
    vikend.append(i)

bins=25         
plt.figure()
plt.hist([radni_tj, vikend], bins, color=['blue','red'])
plt.title(" Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda")
plt.xlabel("Koncentracija tijekom tjedna")
plt.ylabel("Broj čestica PM10")
plt.legend(["Radni tjedan","Vikend"], loc=1)
