# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 22:30:03 2021

@author: nagy
"""

# Zadatak 2
# Napišite programski kod koji će iscrtati sljedeće slike za mtcars skup podataka:
# 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
# 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
# 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
# potrošnju od automobila s automatskim mjenjačem?
# 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
# mjenjačem. 

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

mtcars = pd.read_csv('../resources/mtcars.csv')

# 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
cilindar4= mtcars[mtcars.cyl == 4]   #samo auti koji imaju 4 cilindra
cilindar6= mtcars[mtcars.cyl == 6]
cilindar8= mtcars[mtcars.cyl == 8]

index1=np.arange(0,len(cilindar4),1)  #radi niz od 0 do broja elemenata u cilindar4 s korakom 1
index2=np.arange(0,len(cilindar6),1) 
index3=np.arange(0,len(cilindar8),1) 

width = 0.3 #sirina izmedu kako bi se iybjeglo preklapanje stupaca

plt.figure()

plt.bar(index1, cilindar4["mpg"],width, color="r")
plt.bar(index2 + width, cilindar6["mpg"],width, color="g")
plt.bar(index3 + 2*width, cilindar8["mpg"],width, color="b")
plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
plt.xlabel('auto')
plt.ylabel('mpg')
plt.grid(axis='y',linestyle='--')
plt.legend(['4 cilindra','6 cilindara','8 cilindara'], loc=1) #loc=1 --gore desno 

# 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
tezina4=[]
tezina6=[]
tezina8=[]

for i in cilindar4["wt"]:
    tezina4.append(i)    #niz u koji spremamo vrijednosti tezine vozila s 4 cilindra

for i in cilindar6["wt"]:
    tezina6.append(i)  

for i in cilindar8["wt"]:
    tezina8.append(i)  

plt.figure()
plt.boxplot([tezina4, tezina6, tezina8], positions = [4,6,8])  #predajemo mu 3 niza i poziciju za svaki
plt.title("Tezina automobila s 4, 6 i 8 cilindara")
plt.xlabel('Broj klipova')
plt.ylabel('Tezina wt')
plt.grid(axis='y',linestyle='--')

# 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
# potrošnju od automobila s automatskim mjenjačem?
automatsko=mtcars[(mtcars.am== 0)]
potrosnja_aut=[]
for i in automatsko["mpg"]:
    potrosnja_aut.append(i)
    
rucno=mtcars[(mtcars.am== 1)]
potrosnja_rucno=[]
for i in rucno["mpg"]:
    potrosnja_rucno.append(i)
    
plt.figure()
plt.boxplot([potrosnja_rucno, potrosnja_aut], positions = [0,1])
plt.title("Potrosnja automobila s rucnim vs. automatskim mjenjacem")
plt.ylabel('miles per gallon')
plt.xlabel('1=rucni mjenjac, 0=automatski mjenjac')
plt.grid(axis='y',linestyle='--')

# 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
# mjenjačem. 
ubrzanje_a=[]
for i in automatsko["qsec"]:  
    ubrzanje_a.append(i)
    
snaga_a=[]  
for i in automatsko["hp"]:  
    snaga_a.append(i)
    
ubrzanje_r=[]    
for i in rucno["qsec"]:  
    ubrzanje_r.append(i)
    
snaga_r=[]    
for i in rucno["hp"]:  
    snaga_r.append(i)

plt.figure()
plt.scatter(ubrzanje_a, snaga_a, marker='+')  
plt.scatter(ubrzanje_r, snaga_r, marker='^', facecolors='none', edgecolors='r')  #trokutići
plt.title("Odnos ubrzanja i snage automobila s rucnim u odnosu na automatski mjenjac")
plt.ylabel('Snaga - hp')
plt.xlabel('Ubrzanje - qsec')
plt.legend(["Automatski mjenjac","Rucni mjenjac"])
plt.grid(linestyle='--')
