# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 20:15:35 2021

@author: nagy
"""
# Zadatak 1 
# Za mtcars skup podataka (nalazi se rusu_lv_2019_20/LV3/resources) napišite programski kod koji će
# odgovoriti na sljedeća pitanja:
# 1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
# 2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
# 3. Kolika je srednja potrošnja automobila sa 6 cilindara?
# 4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
# 5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
# 6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
# 7. Kolika je masa svakog automobila u kilogramima? 

import pandas as pd

mtcars = pd.read_csv('../resources/mtcars.csv')

print("1. Kojih 5 automobila ima najveću potrošnju?")
print(mtcars.sort_values(by=['mpg'], ascending = True).head(5)) #head se koristi za prvih 5 jer mpg predstavlja milje po galonu te najveći potrošači imaju najmanji mpg

print("2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?")
osamCilindra = mtcars[mtcars.cyl == 8]  #DataFrame= auti sa 8 cilindara i svi njihovi podaci
print(osamCilindra.sort_values(by=['mpg'], ascending = True).tail(3))

print("3. Kolika je srednja potrošnja automobila sa 6 cilindara?")
sestCilindra = mtcars[mtcars.cyl == 6] 
sr_potrosnja6Cilindra = sestCilindra["mpg"].mean()
print(sr_potrosnja6Cilindra)

print("4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?")
cetiriCilindra = mtcars[(mtcars.cyl == 4) & (mtcars.wt>2) & (mtcars.wt<2.2)]
sr_potrosnja4_masa = cetiriCilindra["mpg"].mean()
print(sr_potrosnja4_masa)

print("5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?")
mjenjac = mtcars.groupby('am').car #.car(ili bilo koji sutpac) je potrebno kako bi grupirao samo za jedan stupac
print(mjenjac.count()) #count() broji jedinice i nule

print("6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?")
auti = mtcars[(mtcars.am == 0) & (mtcars.hp>100)]
print(auti["car"].count()) #broji po stupcu

print("7. Kolika je masa svakog automobila u kilogramima?")
pound2kg=0.45359237    # 1 pound=0.45359237 kg
redni_broj = 0
for wt in mtcars["wt"]:   
    wtValue= wt * 1000            #* 1000 da se dobije pound
    wt_kg_Value = wtValue*pound2kg         
    print("%d. auto: %.3f kg" % (redni_broj, wt_kg_Value))
    redni_broj = redni_broj + 1 
