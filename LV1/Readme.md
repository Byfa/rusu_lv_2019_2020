Tijekom vježbe naučili smo kako koristiti git kako bi svake promjene koda mogli pratiti i spremati, 
	te ukoliko je poželjno, možemo se vratiti na bilo koju prethodnu commitanu verziju.

	Zad1 - Prvi zadatak zahtjeva običan upis sa strane korisnika te množenje i ispis umnoška.
	Zad2 - Zadatak zahtjeva unos od korisnika koji testiramo s našim testnim slučajevima i prema tome ispisujemo odgovarajuću ocjenu.
		Ako ocjena je van zadanog intervala, treba se obavjestiti korisnika, te ako je unesen bilo koji drugi znak osim errora, moramo handlat to.
	Zad3 - Treći zadatak zahtjeva da se množenje iz prvog zadatka odvija u odvojenoj funkciji što znači da korisnikov 
		unos predajemo kao parametre toj funkciji te onda ili vraćamo vrijednost umnoška ili ga odmah ispisujemo.
	Zad4 - Ovaj zadatak zahtjeva da omogućimo korisniku beskonačan unos brojeva koji odradimo u while petlji.
		Unos se mora uvijek testirati kako bi vidjeli je li korisnik dao ključnu riječ s kojom se prekida while petlja.
		Ja sam to odradila u except ValueError dijelu koji se priziva ako korisnikov unos nije broj. Ako nije unesen broj,
		niz se uspoređuje s ključnom riječi te ako odgovara, petlja se prekida i izlazi se iz nje. Potom slijedi ispis min, max broja 
		te suma svih upisanih brojeva. Pošto nam nije potrebno znati koji brojevi su uneseni, ne moramo raditi polje koje ih sadrži 
		već je dovoljna jedna varijabla koja će ih sve zbrajati. Max i min smo određivali tako da smo uspoređivali uneseni broj sa početnom 
		vrijednošću max koja je bila 0, i početnom vrijednošću min koja je bila infinitiv.
	Zad5 - Peti zadatak se bavi potragom određenih linija u tekstualnim datotekama. Za to nam je prvo potrebno testiranje postoji li
		uopće datoteka. Ako datoteka postoji moramo prolaziti kroz svaku njenu liniju te testirati nalazi li se naš zadani niz u njoj.
		Ako da, takvu liniju spremamo u polje koje kasnije odvajamo po znaku ":" koje odvaja tekst od broja koji nama treba.
	Zad6 - Šesti zadatak je sličan prethodnom zadatku gdje tražimo redove koji imaju emailove u sebi. Dio maila, dio poslije znaka @ se uzma 
		u obzir te se gleda koliko puta je koji hostname prikazan unutar dictionary-a.