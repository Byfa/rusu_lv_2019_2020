Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
U liniji 3, potrebno je raw_input zamijeniti u input.
U liniji 5, fnamex je izmijenjeno u fname kako i treba biti.
U linijama 7 i 20 se nalazi print funkcija koja se treba ispraviti tako da varijable i tekstualni niz se nalaze unutar zagrada.
U liniji 8 exit() funkciju je potrebno zapisati kao sys.exit() te je potrebno importat biblioteku sys.