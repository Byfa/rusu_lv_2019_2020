# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 15:27:36 2021

@author: student
"""
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

#Lasso regresion
polyl = PolynomialFeatures(degree=15)
xnewl = polyl.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnewl))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnewl)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnewl)))+1:len(xnewl)]

xtrain = xnewl[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewl[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModell = lm.Lasso()   #LASSO regresija
linearModell.fit(xtrain,ytrain)

ytest_p = linearModell.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)


plt.figure(1)
plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
plt.plot(xtest[:,1],ytest,'or',label='test')
plt.legend(loc = 4)

#pozadinska funkcija vs model
plt.figure(2)
plt.plot(x,y_true,label='f')
plt.plot(x, linearModell.predict(xnewl),'r-',label='model')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4)

xtrain = xnewl[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewl[indeksi_test,]
ytest = y_measured[indeksi_test]

MSEtrain = []
MSEtest = []
lambdas = [0.1, 1, 10, 100, 1000]
labels = ['Lambda = 0.1', 'Lambda = 1', 'Lambda = 10', 'Lambda = 100', 'Lambda = 1000']
styles = ['r-', 'b-', 'y-', 'g-', 'k-']

plt.figure()
plt.plot(x,y_true,label='f')
plt.xlabel('x')
plt.ylabel('y')

for i in range(5):
    linearModel = lm.Ridge(lambdas[i])
    linearModel.fit(xtrain, ytrain)
    
    ytest_p = linearModel.predict(xtest)
    ytrain_p = linearModel.predict(xtrain)
    MSEtest.append(mean_squared_error(ytest, ytest_p))
    MSEtrain.append(mean_squared_error(ytrain, ytrain_p))
    
    plt.plot(x, linearModel.predict(xnewl), styles[i], label=labels[i])

plt.plot(xtrain[:,1], ytrain, 'ok', label='train')
plt.legend(loc = 4) 

print(MSEtrain)
print(MSEtest)

plt.show()


