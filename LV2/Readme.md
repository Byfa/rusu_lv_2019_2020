Zad1 - Prvi zadatak zahtjeva znanje otvaranja datoteka te čitanja iz njih. Te stringove koje iščitamo moramo znati odvojiti sa specialnim
		meta znakovima koristeći RE biblioteku. 
Zad2 - U ovom zadatku nadogradili smo prvi zadatak tako da samo određeni stringovi koje testiramo putem meta znakova odgovaraju našem zahtjevu.
Zad3 - U ovom zadatku smo naučili koristeći matplotlib.pyplot kako plotati te prikazati to unutar pythona.
Zad4 - Ovaj zadatak zahtjeva znanje slično prethodnom te trebamo znati što je histogram te kako ga kreirati.
Zad5 - Peti zadatak se bavi učitavanjem .csv datoteke koja sadrži različite podatke o autima. Pošto znamo koji parametar predstavlja što u	
		određenom redoslijedu, potrebno je samo korisiti funkciju scatter kao u trećem zadatku.
Zad6 - Šesti zadatak se bavi učitavanjem slika te namještanjem njihove svjetline.
Zad7 - Dodatni zadatak se bavi simuliranjem bacanja igraće kocke 30 puta. Testirajući pokus 1000 puta računamo srednju vrijednost rezultata
		i standardnu devijaciju.