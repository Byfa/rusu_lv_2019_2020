# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 18:47:08 2021

@author: student
"""

# #Dodatni zadatak: centralni granični teorem 
# Simulirajte bacanje igraće kocke 30 puta. Izračunajte srednju vrijednost rezultata 
# i standardnu devijaciju. Pokus ponovite 1000 puta. Prikažite razdiobu dobivenih 
# srednjih vrijednosti. Što primjećujete? Kolika je srednja vrijednost i standardna 
# devijacija dobivene razdiobe? Što se događa s ovom razdiobom ako pokus ponovite 10000 puta?

import numpy as np
import matplotlib.pyplot as plt

srednja_vrij=[]
st_dev=[]

for i in range (0, 1000):                 
    niz1 = np.random.randint(1, 7, size=30) 
    srednja_vrij.append(np.average(niz1))  
    st_dev.append(np.std(niz1))             

uk_sr_vrij=np.average(srednja_vrij)         
uk_st_dev=np.std(srednja_vrij)             

bins=10       

plt.hist(srednja_vrij, bins, color="green") 
plt.grid(linestyle='--')

print ("Srednja vrijednost dobivene razdiobe:    %.4f" % uk_sr_vrij )
print ("Standardna devijacija dobivene razdiobe: %.4f" % uk_st_dev )
