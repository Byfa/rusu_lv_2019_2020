# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 18:01:17 2021

@author: student
"""
    
# #zad3
# Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne 
# vrijednosti 0 ili 1. Neka 1 označava mušku osobu, a 0 žensku osobu. 
# Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem odgovarajuće distribucije. 
# U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
# standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija 
# sa srednjom vrijednošću 167 cm i standardnom devijacijom 7 cm. Prikažite podatke te 
# ih obojite plavom (1) odnosno crvenom bojom (0). Napišite funkciju koja računa srednju 
# vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
# np.dot). Prikažite i dobivene srednje vrijednosti na grafu."""

import numpy as np                   
import matplotlib.pyplot as plt       

def srVrijednosti():
    sumaVisina_m = np.zeros([])
    np.dot(spol, visina_uk, sumaVisina_m)
    sumaVisina_z = visina_uk.sum() - sumaVisina_m
    srVrijednost_m = sumaVisina_m/spol.sum()
    srVrijednost_z = sumaVisina_z/(n - spol.sum())
    return [srVrijednost_m, srVrijednost_z]

n = 50
spol = np.random.randint(0,2, size = n)
visina_uk =np.zeros(n)
visina_m = []
visina_z = []
j=0
k=0
for i in range(n):
    if spol[i] == 1:
        visina_uk[i] = np.random.normal(180, 7)
    else:
        visina_uk[i] = np.random.normal(167, 7)
       
color = np.array(['r','b'])
plt.scatter(spol, visina_uk, marker = '.', c = color[spol])
srednjeVrijednosti = srVrijednosti()
spolovi = np.array([1, 0])
plt.scatter(spolovi, srednjeVrijednosti, marker = 'x', c = 'black')