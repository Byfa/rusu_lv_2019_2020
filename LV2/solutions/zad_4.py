# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 18:31:21 2021

@author: student
"""

# #zad4
# Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). 
# Pomoću histograma prikažite rezultat ovih bacanja."""

import numpy as np
import matplotlib.pyplot as plt

simulacija = np.random.randint(1,7, size = 100)
plt.hist(simulacija, bins = 15, color = 'cyan')