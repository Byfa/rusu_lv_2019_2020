# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 17:56:52 2021

@author: student
"""

# #zad2
# Na temelju rješenja prethodnog zadatka izdvojite prvi dio adrese (dio ispred @ znaka) samo ako:
# 1. sadrži najmanje jedno slovo a
# 2. sadrži točno jedno slovo a
# 3. ne sadrži slovo a
# 4. sadrži jedan ili više numeričkih znakova (0 – 9)
# 5. sadrži samo mala slova (a-z) 


import re

file = open("../resources/mbox-short.txt", "r")
najmanjeJednoA=[]
tocnoJedanA=[]
nemaA=[]
brojevi=[]
samoMalaSlova=[]
for line in file:
    email = re.findall('\S+@\S+', line)
    if(email):
        index = email[0].index('@')
        if(email[0][0]=='<'):
            name=email[0][1:index]
        else:
            name=email[0][:index]
        if(name):
            if name.count('a') >= 1:
                najmanjeJednoA.append(name)
            if name.count('a') == 1:
                tocnoJedanA.append(name)
            if re.match('[^a]*', name):
              if name.count('a') == 0:
                nemaA.append(name)
            if re.match('.*[0-9]+.*', name):
                brojevi.append(name)
            if re.match('^[a-z]+$',name):
                samoMalaSlova.append(name)
                
print("Stringovi koji sadrze najmanje jedno a", najmanjeJednoA)
print("Stringovi koji sadrze tocno jedno a", tocnoJedanA)
print("Stringovi koji ne sadrze a", nemaA)
print("Stringovi koji sadrze jedan ili više numerickih znakova", brojevi)
print("Stringovi koji sadrze samo mala slova", samoMalaSlova)