# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 18:40:22 2021

@author: student
"""
#U direktoriju rusu_lv_2019_20/LV2/resources nalazi se datoteka mtcars.csv koja 
#sadrži različita mjerenja provedena na 32 automobila (modeli 1973-74). 
#Prikažite ovisnost potrošnje automobila (mpg) o konjskim
#snagama (hp). Na istom grafu prikažite i informaciju o težini pojedinog vozila. 
#Ispišite minimalne, maksimalne i srednje vrijednosti potrošnje automobila. 


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

cars = pd.read_csv('../resources/mtcars.csv')

plt.scatter(cars.hp, cars.mpg, c = cars.wt, cmap = 'copper')
plt.colorbar().set_label('wt')
plt.xlabel('hp')
plt.ylabel('mpg')

print('Minimalna vrijednost potrosnje: ')
print(min(cars.mpg))
print('Maksimalna vrijednost potrosnje: ')
print(max(cars.mpg))
print('Prosjecna vrijednost potrosnje: ')
print(np.average(cars.mpg))