# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 18:41:09 2021

@author: student
"""
#Zadatak 6
#Na temelju primjera 2.8. učitajte sliku 'tiger.png'. 
#Manipulacijom odgovarajuće numpy matrice pokušajte posvijetliti sliku (povećati brightness). 

import matplotlib.image as mpimg
import matplotlib.pyplot as plt

img = mpimg.imread('../resources/tiger.png')
plt.imshow(img)
plt.figure()
img *=3
plt.imshow(img)
