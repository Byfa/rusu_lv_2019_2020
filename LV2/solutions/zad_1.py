# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 17:05:38 2021

@author: student
"""

# #zad1
# #Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći valjane 
# #email adrese te izdvojiti samo prvi dio adrese (dio ispred znaka @). 
# #Koristite odgovarajući regularni izraz. Koristite datoteku mbox-short.txt. 
# #Ispišite rezultat. 

import re

file=open("../resources/mbox-short.txt", "r")
firstPart = []
for line in file:
    email=re.findall('\S+@\S+', line)
    if(email):
        index = email[0].index('@')
        if(email[0][0]=='<'):
            firstPart.append(email[0][1:index])
        else:
            firstPart.append(email[0][:index])
           
print(firstPart)