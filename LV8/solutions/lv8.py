# -*- coding: utf-8 -*-
"""
Created on Sat Jan 29 14:05:10 2022

@author: nagy
"""

from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
import pandas as pd
import numpy as np 
from tensorflow.keras.preprocessing import image_dataset_from_directory
import os
import shutil

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

train_ds = image_dataset_from_directory(directory='archive/Train/',
                                        labels='inferred',
                                        label_mode='categorical',
                                        batch_size=32,
                                        image_size=(48, 48))


test_data=pd.read_csv('archive/Test.csv')
print(test_data)
print(test_data["Path"][0])

model = keras.Sequential()
model.add(keras.Input(shape=(48,48,3)))
model.add(layers.Conv2D(32, kernel_size=(3,3),padding='same', activation="relu"))
model.add(layers.Conv2D(32, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(64, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(64, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv2D(128, kernel_size=(3,3), padding='same', activation="relu"))
model.add(layers.Conv2D(128, kernel_size=(3,3), activation="relu"))
model.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Flatten())
model.add(layers.Dense(units=512, activation='relu'))
model.add(layers.Dropout(rate=0.5))
model.add(layers.Dense(units=43, activation='softmax'))
model.summary()

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit_generator(train_ds, epochs=5)

test_dir="Test_dir"
parent_dir="D:/Dokumenti/Djeca/Andreja/Faks/Diplomski/1.semestar/RUSU/LV/rusu_lv_2019_2020/LV8/solutions/archive"
path=os.path.join(parent_dir,test_dir)
os.mkdir(path)
for i in range(0,43):
    test_dir=str(i)
    parent_dir="D:/Dokumenti/Djeca/Andreja/Faks/Diplomski/1.semestar/RUSU/LV/rusu_lv_2019_2020/LV8/solutions/archive/Test_dir"
    path=os.path.join(parent_dir,test_dir)
    os.mkdir(path)
    
for i in range (0,len(test_data)):
    source="archive/"+str(test_data["Path"][i])
    #print(source)
    num=str(test_data["ClassId"][i])
    destination="archive/Test_dir/"+num
    dest = shutil.copy2(source, destination)
    
test_ds = image_dataset_from_directory(directory='archive/Test_dir/',
                                        labels='inferred',
                                        label_mode='categorical',
                                        batch_size=32,
                                        image_size=(48, 48))


model.save("CNN/")

predict = model.evaluate_generator(test_ds)
print(predict)

conf_matrix=confusion_matrix(test_ds,predict)
plot_confusion_matrix(conf_matrix)


